# -*- coding: utf-8 -*-
"""
Created on Sun May 27 11:26:36 2018

@author: enovi
"""

import nltk

#News analyzer

def main():
    choice = True
    article_text = ''
    c = 'y'
    print('Welcome to News Analyzer!')
    while choice == True:
         c = input('Analyze an article y/n? ')
         if c == 'y':
             article_text = get_text()
             segment_length = get_length()
             analyze_text(article_text,segment_length)
             choice = True
         elif c == 'n':
             choice = False
             
def get_text():
    article_text = ''
    article_text = str(input('Enter the message here: '))
    return article_text

def get_length():
    c = input('Split article into segments y/n? ')
    if c == 'y':
        segment_length = int(input('Enter the number of segments to split the text into: '))
    else:
        segment_length = 1
    if segment_length < 1:
        segment_length = 1
    return segment_length

def analyze_text(article_text,segment_length):
    
    #sourcing, quantitativeness, certainty
    
    s_tuples = [('a','letter'), ('an','article'), ('accepted','responsibility'),
                ('according', 'to'), ('admitted','to'), ('after','the'), ('appears','in'),
                ('article', 'in'), ('appendices', 'contain'), ('asserted','by'), ('before','the'),
                ('characterized','by'), ('cited','by'), ('corpus','of'), ('described', 'in'),
                ('describing','the'), ('denied','the'), ('developed', 'in'), ('disclosed','by'),
                ('documents','are'), ('et','al'), ('given','by'), ('he','predicted'), ('he','said'),
                ('in','literature'), ('journal','article'), ('look','at'), ('main','contribution'),
                ('many','studies'), ('look','up'), ('models','study'), ('motivated','by'),
                ('numerous','models'), ('observations','are'), ('obtained', 'by'), ('posted','by'),
                ('produced','by'), ('provided','by'), ('reference', 'to'), ('referring','to'),
                ('reported','by'), ('report', 'in'), ('supported', 'by'), ('terms','of'),
                ('the','report'),  ('the','source'), ('these','strategies'), ('seminal','work'), 
                ('she','said'), ('she','predicted'), ('supposed','to'), ('recent','work'),
                ('reference', 'article'), ('relevant','information'), ('report','included'),
                ('research','paper'), ('the','conclusion'), ('the','relevant'), ('the','papers'),
                ('this','citation'), ('this','paper'), ('this','review'), ('this','study'),
                ('white','paper')]
    
    q_tuples = [('a','limit'), ('a','model'), ('above','example'), ('analyze','the'),
                ('apply','the'), ('average','of'), ('basic','familiarity'), ('basic','knowledge'),
                ('basic','understanding'), ('be','solved'), ('chart','the'), ('calculate','the'),
                ('compute','the'), ('define','the'), ('determine','the'), ('elementary','concepts'),
                ('elementary','knowledge'), ('empirically','study'), ('establishes','that'),
                ('estimate','the'), ('example','arises'), ('expect','that'), ('expected','value'),
                ('experimental','results'), ('experiments','show'), ('find','out'), ('has','to'),
                ('have','to'), ('is','called'), ('in','comparison'), ('is','described'),
                ('is','related'), ('is','satisfied'), ('is','sufficient'), 
                ('mathematical','definition'), ('mathematical','terms'), ('not','necessarily'),
                ('often','formulated'), ('only','if'), ('popular','approach'),
                ('preceding', 'examples'), ('precise','definition'), ('plot','the'),
                ('proceeds','by'), ('prove','that'), ('proposed','algorithm'), ('proposed','model'),
                ('shown','that'), ('study','conditions'), ('solve','problems'), 
                ('systematic', 'review'), ('that','correlated'), ('the','concept'),
                ('the','construction'), ('the','data'), ('the','expected'), ('the','performance'),
                ('the','quantities'), ('theory','of'), ('these','models'), ('thus','obtain'),
                ('to','consider'), ('to','derive'), ('to','determine'), ('to','examine'),
                ('to','replicate'), ('to','solve'), ('to','solving'), ('useful','to'),
                ('verifies','that')]
    
    c_tuples = [('a','conclusion'), ('a','solution'), ('acknowledged','that'), ('agree','that'),
                ('analyzing','that'), ('are','computed'), ('are','repeated'), ('assume','that'),
                ('be','combined'), ('be','extended'), ('becomes','clear'), ('believe','that'),
                ('clear','that'), ('do','exist'), ('either','observe'), ('empirical','studies'),
                ('establish','that'), ('estimate','the'), ('essentially','leads'), 
                ('evidence','that'), ('existence','of'),  ('facilitate','the'),
                ('feedback','effects'), ('finally','state'), ('first','explanations'),
                ('forecast','that'), ('forecast','the'), ('fully','describe'), ('fully','explain'),
                ('if','they'), ('if','we'), ('implies','that'), ('in','context'), ('in','contrast'),
                ('in','particular'), ('initial','choice'), ('into','account'), ('is','necessary'),
                ('is','statistically'), ('it','describes'), ('make','decisions'),
                ('method','approximates'), ('new','insights'), ('no','assumption'), ('note','that'),
                ('observe','that'), ('original','model'), ('our','experiments'),
                ('outperforms','the'), ('restricting','attention'), ('results','obtained'),
                ('see','that'), ('seem','to'), ('specific','assumption'),
                ('sufficient','information'), ('suggesting','that'), ('the','context'), 
                ('the','importance'), ('the','outcomes'), ('the','results'), 
                ('theoretical','models'), ('these','conditions'), ('they','imply'), ('this','case'),
                ('this','implies'), ('this','situation'), ('underlying','uncertainty'),
                ('wider','consequences'), ('wider','effects')]
    
    s_score  = 0
    q_score  = 0
    c_score  = 0
    
    article_text = article_text.lower()
    article_text = article_text.split()
    article_text = list(nltk.bigrams(article_text))
    
    for pair in article_text:
        for word in pair:
            word = nltk.PorterStemmer().stem(word)
    
    for pair in s_tuples:
        for word in pair:
            word = nltk.PorterStemmer().stem(word)
            
    for pair in q_tuples:
        for word in pair:
            word = nltk.PorterStemmer().stem(word)        
            
    for pair in c_tuples:
        for word in pair:
            word = nltk.PorterStemmer().stem(word)        
    
    section = int(len(article_text) / segment_length)    
    
    for i in range(1,segment_length + 1):
        selected_text = article_text[section * (i-1):section * i]
    
        for pair in selected_text:
            if pair in s_tuples:
                s_score += 1
            elif pair in q_tuples:
                q_score += 1
            elif pair in c_tuples:
                c_score += 1
        
        print('Segment {} of {}.'.format(i,segment_length))
        if (s_score + q_score + c_score) == 0:
            print('No keywords detected.')
        else:
            print('Sourcing score was: ',s_score/len(selected_text))
            print('Quantitative score was: ',q_score/len(selected_text))
            print('Certainty score was: ',c_score/len(selected_text)) 
        #reset variables
        s_score = 0
        q_score = 0            
        c_score = 0
main()